# vimrc

#### 介绍

自用Vim编辑器配置文件


#### 安装教程
因为不同的网络环境和使用人员不同，提供了以下安装脚本：
- `install_cn.sh`：国内网络环境 - 中文
- `install_en_for_china.sh`：国内网络环境 - 英文
- `install_en_for_international.sh`：国际网络环境 - 英文

如果你已经对你的Vim配置文件`~/.vimrc`进行了一些修改和配置，请备份你的配置。**强烈建议于纯净环境下安装使用**。
##### 基础安装

```bash
curl -s https://gitee.com/westjmoon/vimrc/raw/master/install_cn.sh | sh
```

以上若执行无报错，即可使用。


##### 增强安装

```bash
wget https://gitee.com/westjmoon/vimrc/raw/master/install_cn.sh && bash ./install_cn.sh -p
```
配置文件中已包含两个插件配置[Vimcdoc](https://github.com/yianwillis/vimcdoc)、[LightLine](https://github.com/itchyny/lightline.vim)，如需使用，请执行Vim内部命令：`:PlugInstall`安装。



#### 使用说明

- 依赖：[Vim-Plug](https://github.com/junegunn/vim-plug)
- 使用到的插件：[Vimcdoc](https://github.com/yianwillis/vimcdoc)、[LightLine](https://github.com/itchyny/lightline.vim)
- 键位配置说明：
  - `Leader`键为<kbd>Space</kbd>
  - `<Leader>`+<kbd>c</kbd>：显示/关闭 键入命令
  - `<Leader>`+<kbd>x</kbd>：保存当前文件并退出
  - `<Leader>`+<kbd>f</kbd>：目录树显示隐藏文件
  - `<Leader>`+<kbd>w</kbd>：保存对当前文件
  - `<Leader>`+<kbd>q</kbd>：退出当前文件
  - <kbd>s</kbd>：取消关键词搜索高亮

