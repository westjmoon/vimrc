#!/bin/env bash

echo '--INFO-- Check Vim status...'

if [ -e /bin/vim ];then
	echo 'Vim installed.'
else
	echo 'Vim not installed.'
	exit 1
fi

echo '--INFO-- Vim-Plug Installing...'
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo '--INFO-- Install Vim colorscheme "onedark"'
git clone https://github.com/joshdick/onedark.vim.git
cp -rf ./onedark.vim/* ~/.vim
rm -rf ./onedark.vim/

echo '--INFO-- Getting base vimrc config file'
curl -fLo ~/.vimrc https://gitee.com/westjmoon/vimrc/raw/master/base_vimrc 
if [[ $# -gt 0 ]]; then
	for i in $*; do
		if [[ $i = '-p' ]]; then
				echo '--INFO-- Getting plus vimrc config file'
				curl -sf https://gitee.com/westjmoon/vimrc/raw/master/plus_vimrc >> ~/.vimrc
				echo '--INFO-- Complete!! Pleace run VIM, and run command ":PlugInstall"for install Vim`s Plugin.'
		fi
	done
fi