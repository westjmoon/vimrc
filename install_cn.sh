#!/bin/env bash

echo '--INFO-- 检查Vim状态...'

if [ -e /bin/vim ];then
	echo 'Vim已安装.'
else
	echo 'Vim 未安装.'
	exit 1
fi

echo '--INFO-- Vim-Plug 正在安装...'
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.njuu.cf/junegunn/vim-plug/master/plug.vim

echo '--INFO-- 安装 Vim 配色主题 "onedark"'
git clone https://hub.njuu.cf/joshdick/onedark.vim.git
cp -rf ./onedark.vim/* ~/.vim
rm -rf ./onedark.vim/

echo '--INFO-- 修改 Vim-Plug 源'
sed -i -e 's/github\\\.com/hub\\\.njuu\\\.cf/' -e 's/github\.com/hub\.njuu\.cf/' ~/.vim/autoload/plug.vim

echo '--INFO-- 获取基础vimrc配置文件'
curl -fLo ~/.vimrc https://gitee.com/westjmoon/vimrc/raw/master/base_vimrc 
if [[ $# -gt 0 ]]; then
	for i in $*; do
		if [[ $i = '-p' ]]; then
				echo '--INFO-- 获取增强vimrc配置文件'
				curl -sf https://gitee.com/westjmoon/vimrc/raw/master/plus_vimrc >> ~/.vimrc
				echo '--INFO-- 完成安装!! 请运行 VIM, 并执行命令 ":PlugInstall"以完成插件安装.'
		fi
	done
fi